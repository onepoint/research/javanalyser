//Number Of Methods
MATCH (class:Class)
OPTIONAL MATCH (class)-[:TYPE_MEMBER]->(method)
WHERE
    (method:Method OR method:Constructor)
    AND NOT method.implicit
RETURN class.id AS id, class.name AS class, count(distinct(method)) AS nom
