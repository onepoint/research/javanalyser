//Number Of Parents
MATCH (n:Class)
OPTIONAL MATCH (n)-[:SUPER_TYPE|INTERFACE]->(m)
RETURN n.id AS id, n.name AS class, count(distinct(m)) AS nop
