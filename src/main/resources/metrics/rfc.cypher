//Response For a Class
CALL {
    MATCH (class:Class)-[:TYPE_MEMBER]->(member)
    WHERE
        member:Method
        OR member:Constructor
        OR member:Field
    OPTIONAL MATCH (member)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(node)
    RETURN class, collect(node) AS nodes
}
CALL {
    WITH class, nodes
    UNWIND nodes as invocation
    MATCH (invocation)-[:EXECUTABLE_REF]->(executable)
    WHERE
        NOT (executable IN nodes)
        AND NOT executable.shadow
        AND NOT executable.implicit
        // Delete NESTED_TYPE from line 8 relationships and uncomment the following line
        // AND NOT EXISTS(()-[:NESTED_TYPE]->(:Class)-[:TYPE_MEMBER]->(executable))
    RETURN count(distinct(executable)) as noi
}
CALL {
    WITH class
    MATCH (class)-[:TYPE_MEMBER]->(method)
    WHERE
        (method:Method OR method:Constructor)
        AND NOT method.implicit
    RETURN count(distinct(method)) as local_method
} 
RETURN class.id AS id, class.name AS class, noi + local_method AS rfc
ORDER BY rfc DESC