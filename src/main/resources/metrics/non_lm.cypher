//Number Of Nodes in Longest Method
MATCH (class:Class)-->(method)
WHERE
    method:Method
    OR method:Constructor
MATCH (method)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(node)
WHERE
    NOT node:Comment
    AND NOT node:JavaDoc
    AND NOT node:JavaDocTag
WITH class.id AS id, class.name AS class, method.name AS method, count(distinct(node)) AS non_m
RETURN id, class, max(non_m) AS non_lm
