//Maximum Nesting Depth
MATCH (class:Class)
OPTIONAL MATCH (class:Class)-->(method)
WHERE method:Method OR method:Constructor
OPTIONAL MATCH (method)-[:BODY]->(body)-[:STATEMENT]->(statement)
OPTIONAL MATCH p=(statement)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(node)
RETURN class.id AS id, class.name AS class, max(reduce(nle = 0, n IN nodes(p) | nle + (CASE WHEN n:Block THEN 1 WHEN EXISTS((n:If)<--(:Case)) THEN 1 ELSE 0 END))) AS mnd
