//Coupling Beetween Objects Inverse
MATCH (class:Class)
CALL {
    WITH class
    MATCH (class)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(node)
    RETURN DISTINCT node
}
CALL {
    WITH node
    MATCH (node)-[:ACCESSED_TYPE|ANNOTATION_TYPE|BOUNDING_TYPE|CAST|INTERFACE|MULTI_TYPE|SUPER_TYPE|THROWN|TYPE_ARGUMENT|TYPE]->(otherClass)
    RETURN otherClass
UNION
    WITH node
    MATCH (node)-[:EXECUTABLE_REF|VARIABLE]->()<-[:TYPE_MEMBER]-(otherClass)
    RETURN otherClass
UNION
    WITH node
    MATCH (node)-[:ACCESSED_TYPE|ANNOTATION_TYPE|BOUNDING_TYPE|CAST|INTERFACE|MULTI_TYPE|SUPER_TYPE|THROWN|TYPE_ARGUMENT|TYPE *]->(:ArrayTypeReference)-[:ACCESSED_TYPE|ANNOTATION_TYPE|BOUNDING_TYPE|CAST|INTERFACE|MULTI_TYPE|SUPER_TYPE|THROWN|TYPE_ARGUMENT|TYPE]->(otherClass)
    RETURN otherClass
UNION
    WITH class
    MATCH (class)-[:ACCESSED_TYPE|ANNOTATION_TYPE|BOUNDING_TYPE|CAST|INTERFACE|MULTI_TYPE|SUPER_TYPE|THROWN|TYPE_ARGUMENT|TYPE]->(otherClass)
    RETURN otherClass
}
WITH class, otherClass
WHERE
    (otherClass <> class)
    AND (otherClass:Class OR otherClass:Interface)
    AND NOT otherClass.shadow
    AND NOT EXISTS(()-[:NESTED_TYPE]->(otherClass))
RETURN otherClass.id AS id, otherClass.name AS class, count(distinct(class)) AS cboi
