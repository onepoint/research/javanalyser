//Max Loop Length
MATCH (class:Class)
OPTIONAL MATCH (class)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(loop)
WHERE loop:For OR loop:ForEach OR loop:While OR loop:Do
CALL {
    WITH loop
    MATCH (loop)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(node)
    WHERE
        node:Class
        OR node:Field
        OR node:Constructor
        OR node:Method
        OR node:Block
        OR node:Assignment
        OR node:Invocation
        OR node:If
        OR node:Return
        OR node:For
        OR node:LocalVariable
        OR node:Annotation
        OR node:AnonymousExecutable
        OR node:Assert
        OR node:Break
        OR node:Case
        OR node:Catch
        OR node:Continue
        OR node:Do
        OR node:Enum
        OR node:EnumValue
        OR node:ForEach
        OR node:Switch
        OR node:Throw
        OR node:Try
    RETURN count(distinct(node)) AS loop_length
}
WITH class, loop, loop_length
RETURN class.id AS id, class.name AS class, max(loop_length) AS mll
