//Number Of Local Member
MATCH (class:Class)
OPTIONAL MATCH (class)-[:TYPE_MEMBER]->(member)
WHERE not member.implicit
RETURN class.id AS id, class.name AS class, count(distinct(member)) AS nolm
