//Number Of Ancestors
MATCH (class:Class)
OPTIONAL MATCH (class)-[:SUPER_TYPE|INTERFACE *]->(m)
RETURN class.id AS id, class.name AS class, count(distinct(m)) AS noa
