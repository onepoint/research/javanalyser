//Number of Outgoing Invocations with Inner Classes
CALL {
    MATCH (class:Class)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(node)
    RETURN class, collect(node) AS nodes
}
WITH class, nodes
UNWIND nodes as invocation
MATCH (invocation)-[:EXECUTABLE_REF]->(executable)
WHERE
    NOT (executable IN nodes)
    AND NOT executable.shadow
    AND NOT executable.implicit
RETURN class.id AS id, class.name AS class, count(distinct(executable)) AS noi_ic
