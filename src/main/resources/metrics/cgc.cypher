//Cognitive Complexity
CALL {
    MATCH (class:Class)
    OPTIONAL MATCH path=(class)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(node)
    RETURN class, path, node
	ORDER BY length(path)
}
WITH class, collect(path)[0] AS path, node
WHERE
	node:If
	OR node:Conditional
	OR node:For
	OR node:ForEach
	OR node:While
	OR node:Do
	OR node:Switch
	OR node:Catch
	OR node:Continue
	OR node:Break
	OR (node:Block AND EXISTS((:If)-[:ELSE]->(node)))
	OR (node:BinaryOperator AND (node.operator = "AND" OR node.operator = "OR"))
RETURN class.id AS id, class.name AS class,
	sum(CASE
		WHEN (node:BinaryOperator AND EXISTS((:BinaryOperator {operator:node.operator})-->(node))) THEN 0
		WHEN (node:BinaryOperator) THEN 1
		WHEN (node:Block AND EXISTS((:If)-[:ELSE]->(node))) THEN 1
		WHEN (node:Continue) OR (node:Break) THEN (CASE WHEN EXISTS((node)-[:STATEMENT_ORDER]->()) THEN 1 ELSE 0 END)
		WHEN (node:If AND EXISTS((:If)-[:ELSE]->(:Block {implicit:TRUE})-[:STATEMENT]->(node))) THEN 0
		ELSE reduce(cog_length = 0, n IN nodes(path) | cog_length + CASE
			WHEN (
				(n:If AND NOT EXISTS((:If)-[:ELSE]->(:Block {implicit:TRUE})-[:STATEMENT]->(n)))
				OR n:For
				OR n:Conditional
				OR n:ForEach
				OR n:While
				OR n:Do
				OR n:Switch
				OR n:Catch
				OR n:NewClass
        OR n:Lambda
			) THEN 1 ELSE 0 END)
		END) AS cgc
