//Cyclomatic Complexity
CALL {
    MATCH (class:Class)
    OPTIONAL MATCH (class)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(node)
    RETURN class, node
}
WITH class, node
WHERE
	(node:Constructor AND NOT node.implicit)
	OR node:Method
	OR node:AnonymousExecutable
	OR node:If
	OR node:Conditional
	OR node:For
	OR node:ForEach
	OR node:While
	OR node:Do
	OR (node:Case AND EXISTS((node)-[:EXPRESSION]->()))
	OR node:Catch
	OR (node:BinaryOperator AND (node.operator = "AND" OR node.operator = "OR"))
RETURN class.id AS id, class.name AS class, count(distinct(node)) AS cyc
