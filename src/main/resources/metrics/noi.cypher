//Number of Outgoing Invocations
CALL {
    MATCH (class:Class)-[:TYPE_MEMBER]->(member)
    WHERE
        member:Method
        OR member:Constructor
        OR member:Field
    OPTIONAL MATCH (member)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(node)
    RETURN class, collect(node) AS nodes
}
WITH class, nodes
UNWIND nodes as invocation
MATCH (invocation)-[:EXECUTABLE_REF]->(executable)
WHERE
    NOT (executable IN nodes)
    AND NOT executable.shadow
    AND NOT executable.implicit
    // Delete NESTED_TYPE from line 8 relationships and uncomment the following line
    // AND NOT EXISTS(()-[:NESTED_TYPE]->(:Class)-[:TYPE_MEMBER]->(executable))
RETURN class.id AS id, class.name AS class, count(distinct(executable)) AS noi
