//Number Of Local Attribute and Methods
MATCH (class:Class)
OPTIONAL MATCH (class)-[:TYPE_MEMBER]->(member)
WHERE
    (member:Method OR member:Constructor OR member:Field)
    AND not member.implicit
RETURN class.id AS id, class.name AS class, count(distinct(member)) AS nlam
