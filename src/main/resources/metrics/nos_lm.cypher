//Number Of Statements in Longest Method
MATCH (class:Class)-->(method)
WHERE
    method:Method
    OR method:Constructor
MATCH (method)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(node)
WHERE
    node:Annotation
    OR node:AnonymousExecutable
    OR node:Assert
    OR node:Assignment
    OR node:Block
    OR node:Break
    OR node:Case
    OR node:Catch
    OR node:Class
    OR node:Constructor
    OR node:Continue
    OR node:Do
    OR node:Enum
    OR node:EnumValue
    OR node:Field
    OR node:For
    OR node:ForEach
    OR node:If
    OR node:Invocation
    OR node:LocalVariable
    OR node:Method
    OR node:Return
    OR node:Switch
    OR node:Throw
    OR node:Try
WITH class.id AS id, class.name AS class, method.name AS method, count(distinct(node)) AS nos_m
RETURN id, class, max(nos_m) AS nos_lm
