//Depth of Inheritance Tree
MATCH (class:Class)
OPTIONAL MATCH p=(class)-[:SUPER_TYPE *0..]->()
RETURN class.id AS id, class.name AS class, max(length(p)) AS dit
