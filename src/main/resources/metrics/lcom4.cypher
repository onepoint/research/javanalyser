// Project a graph for lcom4 computation
CALL gds.graph.project.cypher(
    'lcom4_projection',
    'MATCH (class:Class)-[:TYPE_MEMBER]->(member) WHERE (member:Method OR member:Field) AND NOT member.implicit RETURN id(member) AS id',
    'MATCH (method:Method)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(field:Field) RETURN id(method) AS source, id(field) AS target UNION MATCH (class:Class)-[:TYPE_MEMBER]->(method:Method)-[:ANNOTATION|ARGUMENT|ASSIGNED|ASSIGNMENT|BODY|CASE|CATCH|CONDITION|DEFAULT_EXPRESSION|DIMENSION|ELSE|EXPRESSION|FINALIZER|FOREACH_VARIABLE|FOR_INIT|FOR_UPDATE|LEFT_OPERAND|NESTED_TYPE|PARAMETER|RIGHT_OPERAND|STATEMENT|TARGET|THEN|TYPE_MEMBER|TYPE_PARAMETER|VALUE *0..]->(invocation:Invocation)-[:EXECUTABLE_REF]->(executable:Method) WHERE NOT executable.shadow AND NOT executable.implicit AND (NOT EXISTS((invocation)-[:TARGET]->()) OR EXISTS((invocation)-[:TARGET]->(:TypeAccess)-[:ACCESSED_TYPE]->(class))) RETURN id(method) AS source, id(executable) AS target',
    { validateRelationships: false })
YIELD graphName

// Lack of Cohesion in Methods
CALL gds.wcc.stream(graphName)
YIELD nodeId, componentId
WITH gds.util.asNode(nodeId) AS method, componentId
WHERE
    method:Method
    // Rough, but works for the moment
    AND NOT ((method.name STARTS WITH 'get') OR (method.name STARTS WITH 'set'))
MATCH (class:Class { shadow: false })-[:TYPE_MEMBER]->(method)
RETURN class.id AS id, class.name AS class, count(distinct(componentId)) AS lcom4
