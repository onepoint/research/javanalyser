//Schema
MATCH (n)-[r]->(m)
UNWIND labels(n) AS Source
UNWIND labels(m) AS Target
WITH Source, r, Target
WHERE Source <> "Element" AND Target <> "Element"
RETURN DISTINCT Source, type(r) AS Relationship, Target, count(*) AS Count
ORDER BY Source, Relationship, Target
