//Classes
MATCH (class:Class)
WHERE
    NOT class.shadow
    AND NOT EXISTS(()-[:NESTED_TYPE]->(class))
RETURN class.id AS id, class.qualifiedName AS qualifiedName, class.name AS name
ORDER BY qualifiedName
