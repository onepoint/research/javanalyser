MATCH (n)
WHERE n.brokenReference = True
RETURN labels(n)[1] AS label, n.name AS name, n.path AS path
ORDER BY label, name, path
