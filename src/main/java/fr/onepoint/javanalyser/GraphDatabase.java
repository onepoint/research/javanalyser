package fr.onepoint.javanalyser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.javatuples.Pair;
import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GraphDatabase {

  private static final Logger logger = LoggerFactory.getLogger(GraphDatabase.class);

  private final Driver driver;

  private final String uri = "bolt://localhost:7687";
  private final String user = "neo4j";
  private final String password = "password";

  public GraphDatabase() {
    driver = org.neo4j.driver.GraphDatabase.driver(uri, AuthTokens.basic(user, password));
  }

  public void reset() {
    try (Session session = driver.session()) {
      int deleted;
      do {
        deleted = session.run("MATCH (n) WITH n LIMIT 10000 DETACH DELETE n RETURN COUNT (*)")
            .single().get(0).asInt();
      } while (deleted > 0);
      session.run("CALL gds.graph.list() YIELD graphName "
          + "CALL { WITH graphName CALL gds.graph.drop(graphName) YIELD graphName AS deleted } "
          + "RETURN *");
      session.run("CREATE CONSTRAINT constraint_element_id IF NOT EXISTS "
          + "ON (n:Element) ASSERT n.id IS UNIQUE");
    }
  }

  public void load(ProgramGraph graph) {

    // Create all nodes
    var programElements = graph.getProgramElements();
    for (var entry : programElements.entrySet()) {
      logger.info("Loading nodes " + entry.getKey() + " (" + entry.getValue().size() + ")");
      createNodes(entry.getKey(), entry.getValue());
    }

    // Add relations
    var relations = graph.getRelations();
    for (var entry : relations.entrySet()) {
      logger.info("Loading relations " + entry.getKey() + " (" + entry.getValue().size() + ")");
      createRelations(entry.getKey(), entry.getValue());
    }
  }

  private void createNodes(String label, List<ProgramElement> nodes) {
    var batch = nodes.stream()
        .map(this::map)
        .collect(Collectors.toList());
    String query = String.format(
        "UNWIND $batch as row "
            + "CREATE (n:Element:%s) "
            + "SET n += row",
        label);
    driver.session().run(query, Map.of("batch", batch));
  }

  private Map<String, Object> map(ProgramElement programElement) {
    Map<String, Object> map = new HashMap<>();
    map.put("id", programElement.id);
    map.put("path", programElement.path());
    map.put("name", programElement.name());
    map.put("qualifiedName", programElement.qualifiedName());
    map.put("file", programElement.file());
    map.put("line", programElement.line());
    map.put("operator", programElement.operator());
    map.put("implicit", programElement.isImplicit());
    map.put("brokenReference", programElement.isBroken());
    map.put("shadow", programElement.isShadow());
    return map;
  }

  private void createRelations(String type,
      List<Pair<ProgramElement, ProgramElement>> relations) {
    var batch = relations.stream()
        .map(pair -> Map.of(
            "source", pair.getValue0().id,
            "target", pair.getValue1().id))
        .collect(Collectors.toList());
    var query = String.format(
        "UNWIND $batch as row "
            + "MATCH (s:Element {id: row.source}) "
            + "MATCH (t:Element {id: row.target}) "
            + "CREATE (s)-[:%s]->(t)",
        type);
    driver.session().run(query, Map.of("batch", batch));
  }

  public List<String> dump(String query) {
    return driver.session().run(query).stream()
        .map(record -> record.values().toString())
        .sorted()
        .collect(Collectors.toList());
  }

  public Result run(String query) {
    return driver.session().run(query);
  }
}
