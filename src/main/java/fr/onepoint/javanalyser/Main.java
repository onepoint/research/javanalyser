package fr.onepoint.javanalyser;

import java.io.IOException;
import java.nio.file.Path;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  private static final Logger logger = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {

    var options = new Options();

    var pathOption = new Option("p", "path", true, "project path to analyse");
    pathOption.setRequired(true);
    options.addOption(pathOption);

    var javaVersionOption = new Option("j", "java-version", true, "java version of the program");
    options.addOption(javaVersionOption);

    var classpathOption = new Option("c", "classpath", true, "dependencies of the project");
    options.addOption(classpathOption);

    var mavenOption = new Option("m", "maven", false, "for a maven project");
    options.addOption(mavenOption);

    var parser = new DefaultParser();
    var formatter = new HelpFormatter();

    try {
      var cmd = parser.parse(options, args);
      var path = getCanonicalPath(cmd, pathOption);
      var javaVersion = getJavaVersion(cmd, javaVersionOption);
      var classpath = getClasspath(cmd, classpathOption);
      var maven = cmd.hasOption(mavenOption);
      new Javanalyser().scan(path, javaVersion, classpath, maven);
    }

    // CLI parsing exception
    catch (ParseException e) {
      logger.error("There was an error while parsing the arguments: " + e.getMessage());
      formatter.printHelp("Javanalyser", options);
      System.exit(1);
    }

    // Other exception
    catch (Exception e) {
      logger.error(e.getMessage());
      System.exit(1);
    }
  }

  private static String getCanonicalPath(CommandLine cmd, Option pathOption) {
    try {
      var path = cmd.getOptionValue(pathOption);
      return Path.of(path).toFile().getCanonicalPath();
    } catch (IOException e) {
      throw new RuntimeException("There was an error with the path.");
    }
  }

  private static String[] getClasspath(CommandLine cmd, Option classpathOption) {
    var classpath = cmd.getOptionValues(classpathOption);
    return (classpath == null) ? new String[]{} : classpath;
  }

  private static int getJavaVersion(CommandLine cmd, Option javaVersionOption) {
    try {
      var javaVersion = cmd.getOptionValue(javaVersionOption);
      return Integer.parseInt(javaVersion);
    } catch (NumberFormatException e) {
      logger.warn("The java version should be an integer, using 11 by default.");
      return 11;
    }
  }

}
