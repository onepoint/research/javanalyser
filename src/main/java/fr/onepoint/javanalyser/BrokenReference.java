package fr.onepoint.javanalyser;

import spoon.reflect.declaration.CtElement;

public class BrokenReference extends ProgramElement {

  public final CtElement element;
  private final String path;
  private final String name;
  private final String class_;

  /**
   * Create a broken program element with some basic information.
   *
   * @param path         Unique identifier for the program element (used for references)
   * @param name         Simple name of the program element
   * @param elementClass Class of the program element
   */
  BrokenReference(String path, String name, Class<? extends CtElement> elementClass) {
    super(null);
    this.element = null;
    this.path = path;
    this.name = name;
    this.class_ = elementClass.getSimpleName().substring(2);
  }

  @Override
  public boolean isValidChild() {
    return false;
  }

  @Override
  public CtElement parent() {
    return null;
  }

  @Override
  public String roleInParent() {
    return null;
  }

  @Override
  public String path() {
    return path;
  }

  @Override
  public String class_() {
    return class_;
  }

  @Override
  public String name() {
    return name;
  }

  @Override
  public String qualifiedName() {
    return null;
  }

  @Override
  public String file() {
    return null;
  }

  @Override
  public Integer line() {
    return null;
  }

  @Override
  public String operator() {
    return null;
  }

  @Override
  public boolean isImplicit() {
    return false;
  }

  @Override
  public boolean isBroken() {
    return true;
  }

  @Override
  public boolean isShadow() {
    return true;
  }
}
