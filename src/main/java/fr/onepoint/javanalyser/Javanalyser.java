package fr.onepoint.javanalyser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spoon.Launcher;
import spoon.MavenLauncher;
import spoon.MavenLauncher.SOURCE_TYPE;
import spoon.reflect.CtModel;

public class Javanalyser {

  private static final Logger logger = LoggerFactory.getLogger(Javanalyser.class);

  //todo: add an only-metrics option!
  public void scan(String path, int javaVersion, String[] classpath, boolean maven) {

    logger.info("Start Javanalyser on " + path);

    var model = getModel(path, javaVersion, classpath, maven);
    var graph = computeGraph(model);
    graph.scanOrphansAncestors();

    var database = new GraphDatabase();
    var output = new Output(database);
    database.reset();
    database.load(graph);
    output.exportCsv();

    logger.info("End");
  }

  private CtModel getModel(String path, int javaVersion, String[] classpath, boolean maven) {
    Launcher spoon;
    if (maven) {
      logger.warn("Maven mode, ignoring java-version and classpath if any.");
      spoon = new MavenLauncher(path, SOURCE_TYPE.ALL_SOURCE);
    } else {
      spoon = new Launcher();
      spoon.addInputResource(path);
      spoon.getEnvironment().setComplianceLevel(javaVersion);
      spoon.getEnvironment().setSourceClasspath(classpath);
    }
    spoon.buildModel();
    return spoon.getModel();
  }

  private ProgramGraph computeGraph(CtModel model) {
    var scanner = new Scanner();
    model.getRootPackage().accept(scanner);
    return scanner.getGraph();
  }

}
