package fr.onepoint.javanalyser;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.javatuples.Pair;
import spoon.reflect.path.CtRole;

public class ProgramGraph {

  protected Map<String, ProgramElement> programElements = new HashMap<>();
  protected Set<Relation> relations = new HashSet<>();

  public void add(CtRole role, ProgramElement source, ProgramElement target) {
    add(role.name(), source, target);
  }

  public void add(String role, ProgramElement source, ProgramElement target) {
    // It doesn't matter if programElements has already a value associated for the key,
    // because the path is unique for a CtElement, thus for the corresponding ProgramElement,
    // as a consequence the value will be replaced by an identical ProgramElement.
    programElements.put(source.path(), source);
    programElements.put(target.path(), target);
    relations.add(new Relation(role, source.path(), target.path()));
  }

  public void scanOrphansAncestors() {

    var childElements = programElements.values().stream()
        .filter(ProgramElement::isValidChild)
        .collect(Collectors.toCollection(ArrayDeque::new));

    while (childElements.size() > 0) {
      var child = childElements.removeFirst();
      var parent = new ProgramElement(child.parent());
      var role = child.roleInParent();
      var relation = new Relation(role, parent.path(), child.path());
      if (!relations.contains(relation)) {
        if (parent.isValidChild()) {
          childElements.addLast(parent);
        }
        add(role, parent, child);
      }
    }
  }

  public Map<String, List<ProgramElement>> getProgramElements() {

    long id = 0;
    var result = new HashMap<String, List<ProgramElement>>();
    for (var programElement : programElements.values()) {
      programElement.id = id++;
      var key = programElement.class_();
      result.putIfAbsent(key, new ArrayList<>());
      result.get(key).add(programElement);
    }

    return result;
  }

  public HashMap<String, List<Pair<ProgramElement, ProgramElement>>> getRelations() {

    var result = new HashMap<String, List<Pair<ProgramElement, ProgramElement>>>();
    for (var relation : relations) {
      var name = relation.name;
      var source = programElements.get(relation.source);
      var target = programElements.get(relation.target);
      result.putIfAbsent(name, new ArrayList<>());
      result.get(name).add(Pair.with(source, target));
    }

    return result;
  }

  private static class Relation {

    public final String name;
    public final String source;
    public final String target;

    public Relation(String name, String source, String target) {
      this.name = name;
      this.source = source;
      this.target = target;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }

      var relation = (Relation) o;
      return name.equals(relation.name)
          && source.equals(relation.source)
          && target.equals(relation.target);
    }

    @Override
    public int hashCode() {
      return Objects.hash(name, source, target);
    }
  }
}
