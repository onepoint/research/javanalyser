package fr.onepoint.javanalyser;

import spoon.reflect.CtModelImpl.CtRootPackage;
import spoon.reflect.code.CtBinaryOperator;
import spoon.reflect.code.CtUnaryOperator;
import spoon.reflect.declaration.CtAnonymousExecutable;
import spoon.reflect.declaration.CtConstructor;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtNamedElement;
import spoon.reflect.declaration.CtShadowable;
import spoon.reflect.declaration.CtType;
import spoon.reflect.factory.ModuleFactory.CtUnnamedModule;
import spoon.reflect.reference.CtReference;

public class ProgramElement {

  public final CtElement element;
  private final String path;
  public long id; // used by the GraphDatabase

  ProgramElement(CtElement element) {
    this.element = element;
    this.path = path();
  }

  public boolean isValidChild() {
    return !(element instanceof CtReference) && (parent() != null) && (roleInParent() != null);
  }

  public CtElement parent() {
    try {
      return element.getParent();
    } catch (Exception e) {
      return null;
    }
  }

  public String roleInParent() {
    try {
      return element.getRoleInParent().name();
    } catch (Exception e) {
      return null;
    }
  }

  public String path() {

    // Mitigate performance issue by storing the results
    if (path != null) {
      return path;
    }

    if (element instanceof CtRootPackage) {
      return ".rootPackage";
    } else if (element instanceof CtUnnamedModule) {
      return ".rootModule";
    }

    // fixme: performance problem
    try {
      return element.getPath().toString().replaceAll("#", ".");
    } catch (Exception exception) {
      return element.toString();
    }
  }

  public String class_() {

    if (element instanceof CtRootPackage) {
      return "Package";
    } else if (element instanceof CtUnnamedModule) {
      return "Module";
    }

    //todo: include Operator type in the class name? (cf operator())

    var class_ = element.getClass().getSimpleName();
    return class_.substring(2, class_.length() - 4);
  }

  public String name() {

    if (element instanceof CtRootPackage) {
      return "RootPackage";
    } else if (element instanceof CtUnnamedModule) {
      return "RootModule";
    }

    String name;
    if ((element instanceof CtNamedElement) && !(element instanceof CtAnonymousExecutable)) {
      name = ((CtNamedElement) element).getSimpleName();
    } else {
      name = element.getClass().getSimpleName();
      name = name.substring(2, name.length() - 4);
    }
    return name;
  }

  public String qualifiedName() {

    if ((element instanceof CtType<?>)) {
      return ((CtType<?>) element).getQualifiedName();
    } else if (element instanceof CtMethod<?>) {
      var qualifiedClass = ((CtMethod<?>) element).getDeclaringType().getQualifiedName();
      return qualifiedClass + "." + ((CtMethod<?>) element).getSignature();
    } else if (element instanceof CtConstructor<?>) {
      return ((CtConstructor<?>) element).getSignature();
    }

    return null;
  }

  public String file() {
    if (element.getPosition().getFile() != null) {
      return element.getPosition().getFile().getAbsolutePath();
    } else {
      return null;
    }
  }

  public Integer line() {
    if (element.getPosition().getFile() != null) {
      return element.getPosition().getLine();
    } else {
      return null;
    }
  }

  public String operator() {
    if (element instanceof CtBinaryOperator) {
      return ((CtBinaryOperator<?>) element).getKind().toString();
    } else if (element instanceof CtUnaryOperator) {
      return ((CtUnaryOperator<?>) element).getKind().toString();
    } else {
      return null;
    }
  }

  public boolean isImplicit() {
    return element.isImplicit();
  }

  public boolean isBroken() {
    return false;
  }

  public boolean isShadow() {
    if (element instanceof CtShadowable) {
      return ((CtShadowable) element).isShadow();
    } else {
      return false;
    }
  }
}
