package fr.onepoint.javanalyser;

import static spoon.reflect.reference.CtTypeReference.NULL_TYPE_NAME;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.List;
import spoon.reflect.CtModelImpl.CtRootPackage;
import spoon.reflect.code.CtBreak;
import spoon.reflect.code.CtContinue;
import spoon.reflect.code.CtFieldAccess;
import spoon.reflect.code.CtFieldRead;
import spoon.reflect.code.CtFieldWrite;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.code.CtSuperAccess;
import spoon.reflect.code.CtThisAccess;
import spoon.reflect.code.CtVariableAccess;
import spoon.reflect.code.CtVariableRead;
import spoon.reflect.code.CtVariableWrite;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtConstructor;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.path.CtRole;
import spoon.reflect.reference.CtArrayTypeReference;
import spoon.reflect.reference.CtCatchVariableReference;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtLocalVariableReference;
import spoon.reflect.reference.CtPackageReference;
import spoon.reflect.reference.CtParameterReference;
import spoon.reflect.reference.CtTypeParameterReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.CtScanner;

public class Scanner extends CtScanner {

  protected static final String STATEMENT_ORDER = "STATEMENT_ORDER";

  protected Deque<ProgramElement> elementsDeque = new ArrayDeque<>();
  protected CtRole currentRole;
  protected ProgramElement lastProgramElement; //todo: Seems to have to usage... Not very clean...
  protected ProgramGraph programGraph = new ProgramGraph();

  @Override
  protected void enter(CtElement e) {
    if (e instanceof CtRootPackage) {
      return;
    }
    var programElement = new ProgramElement(e);
    if (currentRole != null && elementsDeque.peek() != null) {
      programGraph.add(currentRole, elementsDeque.peek(), programElement);
    }
    elementsDeque.push(programElement);
  }

  @Override
  public void scan(CtRole role, Collection<? extends CtElement> elements) {
    if (elements != null) {
      ProgramElement lastStatement = null;
      for (CtElement e : elements) {
        scan(role, e);
        if (lastStatement != null && isOrdered(role)) {
          programGraph.add(STATEMENT_ORDER, lastStatement, lastProgramElement);
        }
        lastStatement = lastProgramElement;
      }
    }
  }
  //todo: ARGUMENT_ORDER needed? Test with a method taking three integers...

  public boolean isOrdered(CtRole role) {
    return List.of(CtRole.STATEMENT, CtRole.CASE, CtRole.CATCH).contains(role);
  }

  @Override
  public void scan(CtRole role, CtElement element) {
    currentRole = role;
    super.scan(role, element);
  }

  @Override
  protected void exit(CtElement e) {
    if (e instanceof CtRootPackage) {
      return;
    }
    lastProgramElement = elementsDeque.pop();
    if (lastProgramElement.element != e) {
      throw new RuntimeException("Inconsistent Stack");
    }
  }

  protected void addReference(CtElement element) {
    var source = elementsDeque.peek();
    var target = new ProgramElement(element);
    programGraph.add(currentRole, source, target);
  }

  protected void addBrokenReference(String path, String name,
      Class<? extends CtElement> elementClass) {
    var source = elementsDeque.peek();
    var target = new BrokenReference(path, name, elementClass);
    programGraph.add(currentRole, source, target);
  }

  protected ProgramGraph getGraph() {
    return programGraph;
  }

  @Override
  public <T> void visitCtExecutableReference(CtExecutableReference<T> reference) {

    // Add a reference to a valid executable
    var executable = reference.getExecutableDeclaration();
    if (executable != null) {
      addReference(executable);
    }

    // Add a broken reference to a constructor (not found)
    else if (reference.isConstructor()) {
      var path = reference.getSignature();
      var name = reference.getSimpleName();
      addBrokenReference(path, name, CtConstructor.class);
    }

    // Add a broken reference to a method (not found)
    else {
      var path = reference.getSignature();
      var name = reference.getSimpleName();
      addBrokenReference(path, name, CtMethod.class);
    }
  }

  @Override
  public <T> void visitCtFieldReference(CtFieldReference<T> reference) {

    var field = reference.getFieldDeclaration();
    if (field != null) {
      addReference(field);
    }

    //todo: length property from arrays are ignored
    //todo: class property from classes are ignored
  }

  @Override
  public <T> void visitCtLocalVariableReference(CtLocalVariableReference<T> reference) {
    addReference(reference.getDeclaration());
  }

  @Override
  public <T> void visitCtCatchVariableReference(CtCatchVariableReference<T> reference) {
    addReference(reference.getDeclaration());
  }

  @Override
  public void visitCtPackageReference(CtPackageReference reference) {
    // Never used
  }

  @Override
  public <T> void visitCtParameterReference(CtParameterReference<T> reference) {
    addReference(reference.getDeclaration());
  }

  @Override
  public void visitCtTypeParameterReference(CtTypeParameterReference reference) {
    addReference(reference.getDeclaration());
  }

  @Override
  @SuppressWarnings("StatementWithEmptyBody")
  public <T> void visitCtTypeReference(CtTypeReference<T> reference) {

    //todo: sometimes a TypeReference seems useful, maybe should be done as for variable (simple vs complex)
    //  T myvar        => works
    //  T<X> myvar     => works (may be better to have a TypeReference here, but always the same for T<X> ?)
    //  T<X,Y> myvar   => the order of X,Y is missing, how to do it ??
    //  T<U<V>> myvar  => broken, here a TypeReference works : myvar -[type_arg]-> ??? [type_arg]-> V
    //                                                           +-[type]->T                +-[type]->U

    // Add a reference to a valid type
    var type = reference.getTypeDeclaration();
    if (type != null) {
      addReference(type);
      // Actually add this relation to the parent of this reference
      scan(CtRole.TYPE_ARGUMENT, reference.getActualTypeArguments());
    }

    // null is not typed
    else if (NULL_TYPE_NAME.equals(reference.getSimpleName())) {
    }

    // Add a broken reference to an unknown type (not found)
    else {
      var path = reference.getSimpleName();
      var name = reference.getSimpleName();
      var class_ = CtClass.class;  // Very rough, but still better than the generic CtType
      addBrokenReference(path, name, class_);
    }
  }

  @Override
  public void visitCtContinue(CtContinue continueStatement) {
    super.visitCtContinue(continueStatement);
    if (continueStatement.getLabelledStatement() != null) {
      var source = new ProgramElement(continueStatement);
      var target = new ProgramElement(continueStatement.getLabelledStatement());
      programGraph.add(STATEMENT_ORDER, source, target);
      //todo: This is absurd,
      // but some code can follow
      // so the continue will have two following statement_order,
      // maybe a JUMP relationship would be better,
      // moreover I we want to revert to the code...
    }
  }

  @Override
  public void visitCtBreak(CtBreak breakStatement) {
    super.visitCtBreak(breakStatement);
    if (breakStatement.getLabelledStatement() != null) {
      var source = new ProgramElement(breakStatement);
      var target = new ProgramElement(breakStatement.getLabelledStatement());
      programGraph.add(STATEMENT_ORDER, source, target);
      //todo: This is absurd,
      // but some code can follow
      // so the break will have two following statement_order,
      // maybe a JUMP relationship would be better,
      // moreover I we want to revert to the code...
    }
  }

  @Override
  public <T> void visitCtArrayTypeReference(CtArrayTypeReference<T> reference) {
    //todo: Should be rationalised, that is only one Type per possibility should be created
    super.visitCtArrayTypeReference(reference);
  }

  @Override
  public <T> void visitCtConstructor(CtConstructor<T> constructor) {
    if (!constructor.isImplicit()) {
      super.visitCtConstructor(constructor);
    } else {
      // Create the constructor without scanning it
      enter(constructor);
      exit(constructor);
    }
  }

  @Override
  public <T> void visitCtInvocation(CtInvocation<T> invocation) {
    //todo: do not create if implicit ? (beware it does not break the statement order!)
    if (!invocation.isImplicit()) {
      super.visitCtInvocation(invocation);
    } else {
      // Create the invocation without scanning it
      enter(invocation);
      exit(invocation);
    }
  }

  @Override
  public <T> void visitCtThisAccess(CtThisAccess<T> thisAccess) {
    // Ignore CtThisAccess elements
  }

  @Override
  public <T> void visitCtSuperAccess(CtSuperAccess<T> superAccess) {
    // Ignore CtSuperAccess elements
  }

  @Override
  public <T> void visitCtVariableRead(CtVariableRead<T> variableRead) {
    if (simpleVariableAccess(variableRead)) {
      scan(variableRead.getVariable());
      return;
    }
    super.visitCtVariableRead(variableRead);
  }

  @Override
  public <T> void visitCtVariableWrite(CtVariableWrite<T> variableWrite) {
    if (simpleVariableAccess(variableWrite)) {
      scan(variableWrite.getVariable());
      return;
    }
    super.visitCtVariableWrite(variableWrite);
  }

  @Override
  public <T> void visitCtFieldRead(CtFieldRead<T> fieldRead) {
    if (simpleFieldAccess(fieldRead)) {
      scan(fieldRead.getVariable());
      return;
    }
    super.visitCtFieldRead(fieldRead);
  }

  @Override
  public <T> void visitCtFieldWrite(CtFieldWrite<T> fieldWrite) {
    if (simpleFieldAccess(fieldWrite)) {
      scan(fieldWrite.getVariable());
      return;
    }
    super.visitCtFieldWrite(fieldWrite);
  }

  private <T> boolean simpleVariableAccess(CtVariableAccess<T> variableAccess) {
    return variableAccess.getAnnotations().isEmpty()
        && variableAccess.getTypeCasts().isEmpty()
        && variableAccess.getComments().isEmpty();
  }

  private <T> boolean simpleFieldAccess(CtFieldAccess<T> fieldAccess) {
    return simpleVariableAccess(fieldAccess)
        && fieldAccess.getTarget() instanceof CtThisAccess;
  }
}
