package fr.onepoint.javanalyser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.neo4j.driver.Result;
import org.neo4j.driver.Value;
import org.neo4j.driver.internal.value.NullValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Output {

  public static final String ID = "id";
  private static final Logger logger = LoggerFactory.getLogger(Output.class);
  private static final String CSV_DELIMITER = ";";
  private static final String BROKEN_REFERENCES_QUERY = "queries/broken_references.cypher";
  private static final String CLASSES_QUERY = "queries/classes.cypher";
  private static final String SCHEMA_QUERY = "queries/schema.cypher";
  private static final String METRIC_QUERY = "metrics/%s.cypher";
  private static final List<String> METRICS = List.of("cbo", "cbod", "cboi", "cgc", "cyc", "dac",
      "dit", "lcom4", "lpc", "mll", "mnd", "mpc", "nlam", "nle", "noa", "noc", "nod", "noi",
      "noi_ic", "nolm", "nom", "non", "non_am", "non_lm", "non_m", "nop", "nos", "nos_am",
      "nos_deeper_4", "nos_lm", "nos_m", "rfc", "rfc_ic");

  private final GraphDatabase database;

  public Output(GraphDatabase database) {
    this.database = database;
  }

  public void exportCsv() {
    exportMetricsAsCsv();
    exportAsCsv(SCHEMA_QUERY, "schema");
    exportAsCsv(BROKEN_REFERENCES_QUERY, "broken_references");
  }

  //todo: Metrics should be extensively tested!
  private void exportMetricsAsCsv() {
    try {

      var result = run(CLASSES_QUERY);
      var classes = result.stream().collect(Collectors.toMap(
          record -> record.get(ID).asLong(),
          record -> new HashMap<>(record.asMap()),
          (x, y) -> y,
          LinkedHashMap::new
      ));
      var csvKeys = result.keys();
      csvKeys.remove(ID);

      for (var metric : METRICS) {
        logger.info("Querying metric " + metric);
        try {
          var metricResult = run(String.format(METRIC_QUERY, metric));
          csvKeys.add(metric);
          for (var record : metricResult.list()) {
            var id = record.get(ID).asLong();
            if (classes.containsKey(id)) {
              classes.get(id).putIfAbsent(metric, record.get(metric));
            }
          }
        } catch (Exception e) {
          logger.error("Unable to export metric " + metric);
        }
      }

      var lines = new ArrayList<String>();

      lines.add(String.join(CSV_DELIMITER, csvKeys));
      for (var class_ : classes.values()) {
        var values = csvKeys.stream()
            .map(key -> class_.getOrDefault(key, 0))
            .map(value -> (value instanceof NullValue) ? 0 : value)
            .map(Object::toString)
            .collect(Collectors.toList());
        lines.add(String.join(CSV_DELIMITER, values));
      }
      writeFile("./metrics.csv", lines);

    } catch (Exception e) {
      logger.error("Unable to export metrics");
    }
  }

  private void exportAsCsv(String query, String file) {
    try {
      var result = run(query);
      var lines = new ArrayList<String>();
      lines.add(String.join(CSV_DELIMITER, result.keys()));
      result.list().forEach(record -> lines.add(
          record.values().stream()
              .map(Value::toString)
              .collect(Collectors.joining(CSV_DELIMITER))));
      writeFile("./" + file + ".csv", lines);
    } catch (Exception e) {
      logger.error("Unable to export " + file);
    }
  }

  private Result run(String queryFile) throws IOException {
    var query = getResourceFileAsString(queryFile);
    return database.run(query);
  }

  /**
   * Reads given resource file as a string.
   *
   * @param fileName path to the resource file
   * @return the file's contents
   * @throws IOException if read fails for any reason
   */
  private String getResourceFileAsString(String fileName) throws IOException {
    ClassLoader classLoader = ClassLoader.getSystemClassLoader();
    try (InputStream is = classLoader.getResourceAsStream(fileName)) {
      if (is == null) {
        return null;
      }
      try (InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
          BufferedReader reader = new BufferedReader(isr)) {
        return reader.lines().collect(Collectors.joining(System.lineSeparator()));
      }
    }
  }

  /**
   * Writes given lines as a text file.
   *
   * @param path  path of the written file
   * @param lines lines of the text file to be written
   * @throws IOException if write fails for any reason
   */
  private void writeFile(String path, Collection<String> lines) throws IOException {
    var file = Paths.get(path);
    Files.write(file, lines, StandardCharsets.UTF_8);
  }
}
