package fr.onepoint.javanalyser.tests.field_encapsulation;

public class B {

  public void main() {
    var a = new A();
    a.setValue(12);
    var value = a.getValue();
  }

}
