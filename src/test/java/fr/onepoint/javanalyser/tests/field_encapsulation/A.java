package fr.onepoint.javanalyser.tests.field_encapsulation;

public class A {

  public int value;

  public int getValue() {
    return this.value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public A copy(Object a) {
    var copy = new A();
    copy.value = ((int) ((A) a).value);
    return copy;
  }
}
