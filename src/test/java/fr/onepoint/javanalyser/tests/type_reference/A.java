package fr.onepoint.javanalyser.tests.type_reference;

import java.util.List;

public class A {

  String aStringObject;
  Integer anIntegerObject;
  Boolean aBooleanObject;
  int anInt;
  boolean aBoolean;
  B aCustomType;
  B.C aInnerType;
  int[] anIntArray;
  int[][] anIntArrayArray;
  List<String> aStringList;
  List<?> aList;
  List<? extends Number> aNumberList;
  List aRawList;

}
