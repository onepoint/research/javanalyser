package fr.onepoint.javanalyser.tests.generic;

public class A {

  B<Integer> integerB;
  B<String> stringB;
  B<A> ab;
  B<Object> objectB;

}
