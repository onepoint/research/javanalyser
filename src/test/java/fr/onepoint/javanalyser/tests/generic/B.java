package fr.onepoint.javanalyser.tests.generic;

public class B<T> {

  T value;

  public T method1() {
    return value;
  }

  public void method2(T param1, T param2) {
  }

}
