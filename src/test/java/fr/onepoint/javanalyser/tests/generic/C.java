package fr.onepoint.javanalyser.tests.generic;

public class C<T extends Number> {

  T number;

  public T method() {
    return number;
  }

}
