package fr.onepoint.javanalyser.tests.try_catch;

public class A {

  public Integer a;

  int add(Integer b) {
    try {
      return a + b;
    } catch (NullPointerException e) {
      return 0;
    } catch (Exception e) {
      a = 0;
      throw e;
    }
  }

}
