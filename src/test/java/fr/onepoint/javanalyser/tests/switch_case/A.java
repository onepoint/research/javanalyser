package fr.onepoint.javanalyser.tests.switch_case;

public class A {

  int criteria;
  int a;
  int b;

  public void clean_switch() {
    switch (criteria) {
      case 1:
        a = 12;
        b = 12;
        break;
      case 2:
        a = 12;
        b = 6;
        break;
      case 3:
        a = 12;
        b = 4;
        break;
      case 12:
        a = 12;
        b = 1;
        break;
      default:
        a = 0;
        b = 0;
        break;
    }
  }

  public void dirty_switch() {
    switch (criteria) {
      default:
        a = 12;
      case 1:
        b = 22;
        break;
      case 2:
        a = 13;
        return;
      case 3: {
        a = 14;
        b = 24;
        while (b % 3 != 1) {
          b--;
          if (b < a) {
            break;
          }
        }
      }
      case 4:
      case 5:
        a = 15;
        b = 25;
    }
    System.out.println(a);
    System.out.println(a);
  }

}