package fr.onepoint.javanalyser.tests.nested_blocks;

public class A {

  int a;
  int b;
  int c;

  public void nested() {
    if (a > b) {
      a = c * b;
      b = c - a;
      c = 3 * a - 100;
    } else if (b < c) {
      a = 0;
      b = c - a;
      while (b % 2 != 0) {
        c -= 1;
        b = b / 2;
      }
    } else if (c > 0) {
      a = 0;
      b = 0;
      c = 0;
    } else {
      b = a;
      c = a;
    }
    System.out.println(a);
    System.out.println(b);
    System.out.println(c);
  }

}
