package fr.onepoint.javanalyser.tests.singleton;

public class A {

  private static final A instance = new A();

  private A() {
  }

  public static A getInstance() {
    return instance;
  }

  public void someMethod() {
    // This is a method
  }

}
