package fr.onepoint.javanalyser.tests.layer;

public class C {

  private final B b = new B();

  public void method1() {
    b.method1();
    b.method2();
  }

  public void method2() {
    b.method2();
    b.method1();
  }


}
