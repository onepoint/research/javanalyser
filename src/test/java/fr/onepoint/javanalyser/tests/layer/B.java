package fr.onepoint.javanalyser.tests.layer;

public class B {

  private final A a = new A();

  public void method1() {
    a.method1();
    a.method2();
  }

  public void method2() {
    a.method2();
    a.method3();
  }
}
