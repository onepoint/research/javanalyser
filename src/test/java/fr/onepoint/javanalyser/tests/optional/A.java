package fr.onepoint.javanalyser.tests.optional;

import java.util.Optional;

public class A {

  // Cyclomatic Complexity = 1
  String withOptional(String a) {
    return Optional.of(a).orElseGet(this::defaultValue);
  }

  // Cyclomatic Complexity = 2
  String withIf(String a) {
    return a != null ? a : defaultValue();
  }

  String defaultValue() {
    return "Default Value";
  }
}
