package fr.onepoint.javanalyser.tests.inheritance;

public class Main {

  public static void main() {

    var x = new A();
    doTest(x);

    x = new B();
    doTest(x);

    x = new C();
    doTest(x);
  }

  private static void doTest(A x) {
    x.doTest();
  }

}
