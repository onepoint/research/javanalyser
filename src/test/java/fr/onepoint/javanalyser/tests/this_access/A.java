package fr.onepoint.javanalyser.tests.this_access;

public class A {

  public void this_access() {
    this.test();
    var a = new A();
    a.test();
  }

  public void test() {
    // do nothing
  }

}
