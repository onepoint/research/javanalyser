package fr.onepoint.javanalyser;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import spoon.metamodel.ConceptKind;
import spoon.metamodel.Metamodel;

class JavanalyserTest {

  @SuppressWarnings("FieldCanBeLocal")
  private final boolean UPDATE_TEST = false;
  private final GraphDatabase db = new GraphDatabase();

  @BeforeAll
  static void setUp() {
    var tests = "src/test/java/fr/onepoint/javanalyser/tests";
    new Javanalyser().scan(tests, 11, new String[]{}, false);
  }

  static Stream<Arguments> leafLabels() {
    return Metamodel.getInstance().getConcepts().stream()
        .filter(concept -> ConceptKind.LEAF.equals(concept.getKind()))
        .map(concept -> Arguments.of(concept.getName().substring(2)));
  }

  @AfterEach
  public void tearDown() {
    assertFalse(UPDATE_TEST, "UPDATE_TEST should be false to actually run tests.");
  }

  @ParameterizedTest
  @MethodSource("leafLabels")
  public void test_all_nodes_and_their_relationships_for_a_label(String label) throws IOException {
    var query = String.format(
        "MATCH (n:%s) OPTIONAL MATCH (n)-[r]->(m) RETURN n.name, type(r), m.name",
        label);
    var actual = db.dump(query);
    var file = Path.of("src", "test", "resources", label + ".txt").toFile();
    if (UPDATE_TEST) {
      FileUtils.writeLines(file, actual);
    }
    var expected = FileUtils.readLines(file, StandardCharsets.UTF_8);
    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected);
  }
}
