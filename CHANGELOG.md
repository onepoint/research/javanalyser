# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2024-12-18

### Changed

- Round average nodes and average statements by methods metrics to 2 decimals.
- Sort output files for stable comparison.
- Do not visit implicit invocations.

### Fixed

- Count ``Continue`` and ``Break`` statements in cognitive complexity metric.
- Add a ``STATEMENT_ORDER`` relation on labelled jumps (``Continue`` and ``Break``).
- Count invocations instead of executables in message-passing coupling.

## [1.0.0] - 2023-03-04

### Added

- Initial release of Javanalyser.
- Represents code as a graph in a Neo4j 4 database.
- Allowing to easily extract a set of 33 metrics.

[unreleased]: https://gitlab.com/onepoint/research/javanalyser/-/compare/1.1.0...master
[1.1.0]: https://gitlab.com/onepoint/research/javanalyser/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/onepoint/research/javanalyser/-/tree/1.0.0
