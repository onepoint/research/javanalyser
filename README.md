# Javanalyser

Javanalyser represents code as a graph,
depending only on the structure of the code,
and allowing to easily extract data such as metrics.
Javanalyser parses a Java program,
produces a neo4j graph, and outputs metrics in a CSV file.

Javanalyser is a research project developed by [onepoint](https://www.groupeonepoint.com/fr/).

## License

Javanalyser is Free and Open Source, licensed under the MIT license.

## Contributing

Developed with respect to
the [Google's Java Style Guide](https://google.github.io/styleguide/javaguide.html).
